/*
    This file is a part of Avoid Copyright Big Stack of Hay 2020
    
    Avoid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avoid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Avoid.  If not, see <https://www.gnu.org/licenses/>.
*/

var Engine = {};

// Game constructor
Engine.CreateGame = function() {
    var Game = {}; // Initialise temporary game object
    Game.ctx; // Rendering Context
    Game.onUpdate = function() {}; // Logic update
    Game.setup = function () {}; // Game setup
    Game.entityList = {}; // List of all entities created
    Game.effectList = {}; // List of all effects created
    Game.isRunning = true; // The state of the Game

    Game.score = 0; // The score of the Game
    Game.score2 = 0; // The score of the Game
    Game.isMultiplayer = false;

    Game.Player1ShowScore = 0;
    Game.Player2ShowScore = 0;

    Game.frames = 0;
    Game.input = { // Set all keys to false
        up:false, // up key
        down:false, // down key
        left:false, // left key
        right:false, // right key
        up2:false, // up key 2
        down2:false, // down key 2
        left2:false, // left key 2
        right2:false, // right key 2
    }

    Game.displayScore = true;
    Game.gameOver = false;

    Game.level = 1;
    Game.toLevelComplete = 5;
    Game.startlevel = false;

    Game.extraPlayerSpeed = 0;
    Game.extraPlayer2Speed = 0;

    Game.BckMusic = new Audio();
    Game.BckMusic.src = "./audio/Avoid.wav";

    Game.explosionSound = new Audio();
    Game.explosionSound.src = "./audio/explosion_shortened.wav";

    Game.levelSound = new Audio();
    Game.levelSound.src = "./audio/level.wav";

    Game.pelletCollect = new Audio();
    Game.pelletCollect.src = "./audio/pellet.wav";

    Game.GameOverCounter = 0;

    // Game initialiser
    Game.Init = function(ctx, setup, onUpdate) {
        Game.ctx = ctx; // Rendering context
        Game.setup = setup; // Setup function
        Game.onUpdate = onUpdate; // called before update function
        Game.ctx.font = "50px Consolas";
        
        Game.BckMusic.loop = true;
        Game.BckMusic.play();
    }

    // Game update function
    Game.update = function() {
        Game.frames += 1;
        Game.ctx.canvas.width = window.innerWidth; // Update Canvas Width
        Game.ctx.canvas.height = window.innerHeight; // Update Canvas Height
        Game.ctx.clearRect(0, 0, Game.ctx.canvas.width, Game.ctx.canvas.height); // Clear the canvas

        if (Game.isRunning == true) { // Only update if game is running
           
            Game.extraPlayerSpeed -= 1;

            for (var i in Game.entityList) { // delete requested entities
                if (Game.entityList[i].toDelete) {
                    delete Game.entityList[i];
                }
            }

            for (var i in Game.entityList) { // update entities
                Game.entityList[i].update();
            }

            if(Game.toLevelComplete <= 0) {
                Game.level += 1;
                Game.toLevelComplete = Game.level * 3;
                Game.levelText.Reset();

                Game.levelSound.play();

                for (var i in Game.entityList) { // delete requested entities
                    if (Game.entityList[i].type == "player") {
                        if(Game.entityList[i].id == "player") {
                            Game.CreateEffect(Game.entityList[i].x, Game.entityList[i].y, Game.entityList[i].width, Game.entityList[i].height,
                                0, 255, 0);
                        }
                        if(Game.entityList[i].id == "player2") {
                            Game.CreateEffect(Game.entityList[i].x, Game.entityList[i].y, Game.entityList[i].width, Game.entityList[i].height,
                                0, 0, 255);
                        }
                    }
                    if (Game.entityList[i].type == "obstacle") {
                        Game.CreateEffect(Game.entityList[i].x, Game.entityList[i].y, 
                            Game.entityList[i].width, Game.entityList[i].height, 255, 0 ,0);
                        delete Game.entityList[i];
                    }
                }
            }


            if(Game.displayScore) {
                Game.ctx.save();

                if(Game.Player1ShowScore < Game.score) {
                    Game.Player1ShowScore += 1;
                } else {
                    Game.Player1ShowScore = Game.score
                }

                if(Game.Player2ShowScore < Game.score2) {
                    Game.Player2ShowScore += 1;
                } else {
                    Game.Player2ShowScore = Game.score2
                }

                Game.DrawText("Player 1: " + Game.Player1ShowScore, 10, 32, 'white');
                Game.DrawText("Collect: " + Game.toLevelComplete, Game.ctx.canvas.width / 2 - Game.ctx.measureText("Collect: " + Game.toLevelComplete).width / 2, 32, 'white');
                Game.DrawText("Level: " + Game.level, Game.ctx.canvas.width / 2 - Game.ctx.measureText("Level: " + Game.level).width / 2, 64, 'white');
                Game.ctx.font = "30px Anonymous Pro";
                if(Game.isMultiplayer == true) {
                    Game.DrawText("Player 2: " + Game.Player2ShowScore, Game.ctx.canvas.width - Game.ctx.measureText("Player 2: " + Game.Player2ShowScore).width - 10, 32, 'white');
                }
                Game.ctx.restore();
                Game.levelText.Update();
            }
            
            var players = 0;

            for(var i in Game.entityList) { // Loop through entire list of entities
                if (Game.entityList[i].type == "player") { // Test collision only on obstacles
                    players += 1;
                }
            }

            if(players == 0) {
                Game.GameOverCounter += 1;
                if(Game.GameOverCounter >= 120) {
                    Game.GiveGameOver();
                }
            }
        }

        if(Game.frames % 2 == 0) {
            Game.CreateEffect(Math.random() * Game.ctx.canvas.width, Math.random() * Game.ctx.canvas.height, 5, 5, 
            Math.random() * 255, Math.random() * 255, Math.random() * 255);
        }

        for (var i in Game.effectList) {
            Game.effectList[i].Update();
        } 

        for (var i in Game.effectList) {
            if(Game.effectList[i].alpha <= 0) {
                delete Game.effectList[i];
            }
        } 

        Game.onUpdate(); // custom update logic

        requestAnimationFrame(Game.update); // Run update again
    }

    Game.StartEffects = function() {
        //Game.setup();
        //Game.Reset();
        //Game.Start();
        Game.isRunning = false;
        Game.update();
    }

    // function to start game
    Game.Start = function(multiplayer) {
        Game.frames = 0;
        Game.score = 0; // Reset Score
        Game.score2 = 0; // Reset Score
        for (var i in Game.input) {
            Game.input[i] = false; // reset all keys
        }

        if(multiplayer != undefined && multiplayer == true ) {
            Game.isMultiplayer = true;
        } else {
            Game.isMultiplayer = false;
        }

        Game.isRunning = true; // Ensure that the game is running
        Game.setup(); // run custom setup code
        //Game.update(); // Start the update loop

        Game.levelSound.play();
    }

    Game.GiveGameOver = function() {
        Game.displayScore = false;
        Game.isRunning = false; // End the Game
        Game.gameOver = true;
    }

    // Function to reset game
    Game.Reset = function() {
        Game.score = 0; // Reset score
        Game.score2 = 0; // Reset score
        Game.level = 1; // Reset the score
        Game.toLevelComplete = 5;
        Game.isRunning = false; // Ensure that the update loop is not running
        Game.input.up = false; // Reset up key
        Game.input.down = false; // Reset down key
        Game.input.left = false; // Reset left key
        Game.input.right = false; // Reset right key
        Game.entityList = {}; // Delete all entities
        Game.effectList = {}; // Delete all entities
        Game.levelText.Reset();

        Game.Player1ShowScore = 0;
        Game.Player2ShowScore = 0;

        Game.GameOverCounter = 0;

        Game.extraPlayerSpeed = 0;
        Game.extraPlayer2Speed = 0;

        Game.gameOver = false;

        Game.displayScore = true;
        Game.Start(); // Start the game again
    }

    Game.TestEntityCollision = function(entityA, entityB) {
        return entityA.x <= entityB.x+entityB.width // Compare x positions against entity B width
                && entityB.x <= entityA.x+entityA.width // Compare x positions against enity A width
                && entityA.y <= entityB.y + entityB.height // Compare y positions against entity b height
                && entityB.y <= entityA.y + entityA.height; // Compare y positions against entity a height
    }

    Game.DrawText = function(text, x, y, colour, font) {
        Game.ctx.save();
        Game.ctx.fillStyle = colour;
        if(font != undefined)
            Game.ctx.font = font;
        else
            Game.ctx.font = '30px Anonymous Pro';
        Game.ctx.fillText(text, x, y);
        Game.ctx.restore();
    }

    Game.RandomObstacle = function(player) {
        var x = Math.random() * Avoid.ctx.canvas.width;
        var y = Math.random() * Avoid.ctx.canvas.height;

        var Yv = 0;
        var Xv = 0;

        var choosing = false

        while(!choosing) {
            x = Math.random() * Game.ctx.canvas.width;
            y = Math.random() * Game.ctx.canvas.height;

            var choosing = true;
            for (var i in Game.entityList) {
                if (Game.entityList[i].type == 'player') {
                    if(Game.TestEntityCollision({x: x-250, y:y-250, width:500, height:500}, Game.entityList[i])) {
                        choosing = false;
                    }
                }
            }
        }

        

        var angle = 0;
        angle = Math.atan2(y - player.y, x - player.x);

        Yv = Math.sin(angle);
        Xv = Math.cos(angle);

        Game.CreateObstacle(Math.random() * Math.random(), x, y, Game.level + 4, 'rgb(255, 0 ,0)', 32, 32, Xv, Yv);
    }

    // Basic Entity constructor
    // Update callback takes on parameter to represent the entity
    Game.CreateEntity = function(id, type, x, y, speed, colour, width, height, update, direction, Xv, Yv) {
        var e = {}; // Initialise blank entity
        e.id = id; // The entity's unique id can be anything
        e.x = x; // starting x
        e.y = y; // starting y
        e.type = type; // Type of entity (player, coin, obstacle)
        e.speed = speed; // The maximum speed for the entity to move at
        e.speedX = 0; // The speed which the object is moving at on the x axis
        e.speedY = 0; // The speed which the object is moving at on the y axis
        e.colour = colour; // Colour for the rectangle
        e.width = width; // width for the rectangle
        e.height = height; // height for the rectangle
        e.toDelete = false; // if true the game will erase the entity
        e.timer = 0;
        e.direction = 0;

        if (direction !== undefined) {
            e.direction = direction;
        }
        e.onUpdate = update; // update function
        if (e.type == 'obstacle') {
            e.speedX = e.speed;
            e.speedY = e.speed;
        }
        if (e.type == 'bullet') {
            e.speedX = Math.cos(e.direction/180*Math.PI)*5;
            e.speedY = Math.sin(e.direction/180*Math.PI)*5;
            e.speedX = e.speed;
            e.speedY = e.speed;
        }
        if(Xv != undefined) {
            e.speedX = Xv;
        }
        if(Yv != undefined) {
            e.speedY = Yv;
        }
        e.draw = function() { // Draw function
            if((e.type == 'player' && Game.isRunning == false) == false) {
                Game.ctx.save(); // Save ctx settings
                Game.ctx.fillStyle = e.colour; // set the colour for the rectangle to be drawn
                Game.ctx.fillRect(e.x, e.y, e.width, e.height); // draw the rectangle
                Game.ctx.restore(); // Restore saved ctx settings
            }
        }
        e.update = function() {
            e.draw(); // Call the draw function
            e.onUpdate(e); // Call the onUpdate function
        }
        Game.entityList[e.id] = e; // add to the game's entitylist
    }

    // Player Constructor
    Game.CreatePlayer = function(id, x, y, speed, colour, width, height) {
        Game.CreateEntity(id, "player", x, y, speed, colour, width, height, function(p) {
            

            for(var i in Game.entityList) { // Loop through entire list of entities
                if (Game.entityList[i].type == "obstacle") { // Test collision only on obstacles

                    if (Game.TestEntityCollision(p, Game.entityList[i])) { // Test collision with obstacle

                        Game.explosionSound.pause();
                        Game.explosionSound.currentTime = 0;
                        var sound = Game.explosionSound;
                        sound.play();

                        delete Game.entityList[p.id];
                        if(p.id == "player")
                            Game.CreateEffect(p.x, p.y, p.width, p.height, 0, 255, 0);
                        if(p.id == "player2")
                            Game.CreateEffect(p.x, p.y, p.width, p.height, 0, 0, 255);
                    }
                }
            }
            
            // Key presses
            var addon = 1;

            if(p.id == "player") {
                if(Game.extraPlayerSpeed >= 0) {
                    addon = 1.2;
                }
                if (Game.input.up) { // Move up
                    p.speedY -= addon;
                }
                if (Game.input.down) { // Move down
                    p.speedY += addon;
                }
                if (Game.input.left) { // Move left
                    p.speedX -= addon;
                }
                if (Game.input.right) { // Move right
                    p.speedX += addon;
                }
            } else {
                if(Game.extraPlayer2Speed >= 0) {
                    addon = 1.2;
                }
                if (Game.input.up2) { // Move up
                    p.speedY -= addon;
                }
                if (Game.input.down2) { // Move down
                    p.speedY += addon;
                }
                if (Game.input.left2) { // Move left
                    p.speedX -= addon;
                }
                if (Game.input.right2) { // Move right
                    p.speedX += addon;
                }
            }
            p.speedY *= 0.9; // Regulate the y speed
            p.speedX *= 0.9; // Regulate the x speed


            p.y += p.speedY; // Update X position smoothly
            p.x += p.speedX; // Update Y position smoothly
            p.timer++;
            if (p.timer == 20) {
                p.timer = 0;
            }

            if(p.x < 0) {
                p.x = 0;
            }
            if(p.y < 0) {
                p.y = 0;
            }
            if(p.x + p.width > Game.ctx.canvas.width) {
                p.x = Game.ctx.canvas.width - p.width;
            }
            if(p.y + p.height > Game.ctx.canvas.height) {
                p.y = Game.ctx.canvas.height - p.height;
            }
        });
    }

    // Obstacle Constructor
    Game.CreateObstacle = function(id, x, y, speed, colour, width, height, Xv, Yv) {
        Game.CreateEffect(x, y, width, height, 255, 0, 0);

        Game.CreateEntity(id, "obstacle", x, y, speed, colour, width, height, function (o) {
            o.x += o.speedX * o.speed; // Move the obstacle on the x axis
            o.y += o.speedY * o.speed; // Move the obstacle on the y axis

            if(o.x < 0) {
                o.x = 0;
                o.speedX = -o.speedX;
            }
            if(o.y < 0) {
                o.y = 0;
                o.speedY = -o.speedY;
            }
            if(o.x + o.width > Game.ctx.canvas.width) {
                o.x = Game.ctx.canvas.width - o.width;
                o.speedX = -o.speedX;
            }
            if(o.y + o.height > Game.ctx.canvas.height) {
                o.y = Game.ctx.canvas.height - o.height;
                o.speedY = -o.speedY;
            }

            //o.speedX *= 0.9; // Regulate x speed
            //o.speedY *= 0.9; // Regulate y speed
            
        }, 0, Xv, Yv);
    }

    // Coin constructor
    Game.CreateCoin = function(id, x, y, type) {
        var colour = 'rgb(255, 255, 0)';
        if(type == 'speed') {
            var colour = 'rgb(50, 50, 255)';
        }
        if(type == 'explosive') {
            var colour = 'rgb(255, 0, 0)';
        }
        if(type == 'extra') {
            var colour = 'rgb(255, 255, 255)';
        }

        Game.CreateEntity(id, "coin", x, y, 0, colour, 10, 10, function (c) { // Use entity constructor
            for (var i in Game.entityList) { // Loop through entire entity list
                if (Game.entityList[i].type == "player") { // Only test players
                    if (Game.TestEntityCollision(c, {x: Game.entityList[i].x - 5, y: Game.entityList[i].y - 5, 
                        width: Game.entityList[i].height + 10, height: Game.entityList[i].height + 10})) { // Test collision
                        var toAdd = 10;

                        c.toDelete = true; // Set the coin to be deleted
                        Game.RandomObstacle(Game.entityList[i]); // Generate new random  obstacle

                        Game.toLevelComplete -= 1;

                        if(type == 'extra') {
                            toAdd += 20;
                        }

                        if(type == 'speed') {
                            Game.extraPlayerSpeed = 69 * 5;
                        }

                        if(type == 'explosive') {
                            Game.CreateEffect(c.x - 96, c.y - 96, 192, 192, 255, 0, 0);
                            for(var ii in Game.entityList) {
                                if(Game.entityList[ii].type == 'obstacle') {
                                    var ob = Game.entityList[ii];
                                    if(Game.TestEntityCollision({x: c.x - 250 + c.width / 2, y: c.y - 250 + c.height / 2, width: 500, height: 500}, ob)) {
                                        toAdd += 10;
                                        Game.CreateEffect(ob.x, ob.y, ob.width, ob.height, 255, 0, 0);
                                        delete Game.entityList[ii];
                                    }
                                }
                            }

                            Game.explosionSound.pause();
                            Game.explosionSound.currentTime = 0;
                            var sound = Game.explosionSound;
                            sound.play();
                        }

                        Game.pelletCollect.pause();
                        Game.pelletCollect.currentTime = 0;
                        var coinC = Game.pelletCollect;
                        coinC.play();

                        if(Game.entityList[i].id == "player") {
                            Game.score += toAdd;
                            Game.CreateEffect(x, y, 10, 10, 255, 255, 0); //creates an effect
                        } else {
                            Game.score2 += toAdd;
                            Game.CreateEffect(x, y, 10, 10, 255, 255, 0); //creates an effect
                        }

                        break; // Leave the loop
                    }

                    if(Game.frames % 80 == 0) {
                        Game.CreateEffect(x, y, 10, 10, 255, 255, 0); //creates an effect
                    }
                }
            }
        });
    }

    // Bullet Constructor
    Game.CreateBullet = function(id, x, y, direction) {
        /*Game.CreateEntity(id, 'bullet', x, y, 5, 'orange', 10, 10, function(b) {
            b.x += b.speedX;
            b.y += b.speedY;
                        
            if(b.x < 0 || b.x > Game.ctx.canvas.width){
                    b.speedX = -b.speedX;
            }
            if(b.y < 0 || b.y > Game.ctx.canvas.height){
                    b.speedY = -b.speedY;
            }

            for(var i in Game.entityList) {
                if (Game.entityList[i].type == 'obstacle') {
                    if (Game.TestEntityCollision(b, Game.entityList[i])) {
                        Game.entityList[i].toDelete = true; // Remove the obstacle
                        b.toDelete = true; // Remove the bullet
                        return;
                    }
                    
                }
            }
            b.timer++;
            if (b.timer == 500) {
                b.toDelete = true;
            }
        }, direction);*/
    }

    Game.CreateEffect = function(x, y, width, height, r, g, b) {
        var e = {};
        e.x = x;
        e.y = y;
        e.width = width;
        e.height = height;
        e.r = r;
        e.g = g;
        e.b = b;
        e.alpha = 1;

        if(r == 255 && b == 255 && c == 0)
            console.log("hello world");

        e.Update = function() {
            e.x -= e.width * 0.05;
            e.y -= e.height * 0.05;
            e.width += e.width * 0.1;
            e.height += e.height * 0.1;

            e.alpha -= 0.075;

            e.Draw();
        }

        e.Draw = function() {
            Game.ctx.save();

            Game.ctx.fillStyle = "rgba(" + e.r + ", " + e.g + ", " + e.b + ", " + e.alpha + ")";
            Game.ctx.fillRect(e.x, e.y, e.width, e.height);
            
            Game.ctx.restore();
        }

        Game.effectList[Math.random()] = e;
    }

    Game.levelText = {}
    Game.levelText.x = -999;
    Game.levelText.y = 0;
    Game.levelText.Update = function() {
        var Xv = -50;
        if(Game.levelText.x <= Game.ctx.canvas.width / 2 + 96 && Game.levelText.x >= Game.ctx.canvas.width / 2 - 128) {
            Xv = -2;
        }
        
        Game.levelText.x += Xv;
        Game.levelText.Draw();
    };
    Game.levelText.Draw = function() {
        Game.DrawText("Level " + Game.level, Game.levelText.x, Game.levelText.y, 'white', '64px Anonymous Pro');
    }
    Game.levelText.Reset = function() 
    {
        Game.levelText.x = Game.ctx.canvas.width;
        Game.levelText.y = Game.ctx.canvas.height / 2 - 32;
    }

    return Game;
}