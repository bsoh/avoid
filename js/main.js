/*
    This file is a part of Avoid Copyright Big Stack of Hay 2020
    
    Avoid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avoid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Avoid.  If not, see <https://www.gnu.org/licenses/>.
*/

var Avoid = Engine.CreateGame();
var ctx = document.getElementById('ctx').getContext('2d');
var startBtn = document.getElementById('start1player');
var start2Btn = document.getElementById('start2player');
var menuDiv = document.getElementById('menu');
var scoreDiv = document.getElementById('score');
scoreDiv.style.display = '' + 'none';

var copyrightNotices = document.getElementById('copyrightNotices');

var splashDiv = document.getElementById('SplashScreen');
var splashImg = document.getElementById('imgSplash');
var splashOpacity = 0;
var splashCount = 0;

var DisplayedScore = 0;
var DisplayedScore2 = 0;
var DisplayedLevel = 0;

var singlePlayer = true;

var tick = new Audio();
tick.src = "./audio/tick.wav";

Avoid.Init(
    ctx, 
    function () { // Setup
        Avoid.CreatePlayer('player', 45, 45, 10,'rgb(0, 255, 0)', 32, 32);

        if(singlePlayer == false) {
            Avoid.CreatePlayer('player2', Avoid.ctx.canvas.width - 32 - 45, 45, 10,'rgb(0, 0, 255)', 32, 32);
        }

        Avoid.CreateCoin(Math.random(), Math.random() * Avoid.ctx.canvas.width, Math.random() * Avoid.ctx.canvas.height);
        Avoid.ctx.font = "30px Arial";
    },
    function () { // Update
        //console.log('updating!');
        // Check if any coins exist
        if(Avoid.isRunning == true) {
            var coins = 0;
            for (var i in Avoid.entityList) {
                if (Avoid.entityList[i].type == 'coin') {
                    coins ++;
                }
            }
    
            var coins2 = singlePlayer ? 5 : 10;

            if (coins < coins2) {
                var type = Math.random();
                var trueType = "coin";
                if(type <= 0.1) {
                    trueType = 'explosive';
                }
                if(type >= 0.1 && type <= 0.2) {
                    trueType = 'speed';
                }
                if(type >= 0.2 && type <= 0.3) {
                    trueType = 'extra';
                }

                Avoid.CreateCoin(Math.random() * Math.random(), Math.random() * (Avoid.ctx.canvas.width-10), Math.random() * (Avoid.ctx.canvas.height-10), trueType);
            }
        }

        if(Avoid.gameOver) {
            var toDisplay = "";
            if(singlePlayer == true) {
                if(DisplayedScore < Avoid.score) {
                    DisplayedScore += 10;
                    toDisplay = "You Scored " + DisplayedScore + "!"
                    playTick();
                } else {
                    DisplayedScore = Avoid.score;
                    if(DisplayedLevel < Avoid.level) {
                        DisplayedLevel += 1;
                        toDisplay = "You Scored " + DisplayedScore + "!<br>You reached level " + DisplayedLevel + "!";
                        playTick();
                    } else {
                        DisplayedLevel = Avoid.level;
                        toDisplay = "You Scored " + DisplayedScore + "!<br>You reached level " + DisplayedLevel + "!";
                    }
                }
            } else {
                if(DisplayedScore < Avoid.score) {
                    DisplayedScore += 10;
                    playTick();
                    toDisplay = "Player 1 Scored " + DisplayedScore + "!"
                } else {
                    if(DisplayedScore2 < Avoid.score2) {
                        DisplayedScore2 += 10;
                        playTick();
                        toDisplay = "Player 1 Scored " + DisplayedScore + "!<br>Player 2 Scored " + DisplayedScore2 + "!"
                    } else {
                        DisplayedScore = Avoid.score;
                        if(DisplayedLevel < Avoid.level) {
                            DisplayedLevel += 1;
                            playTick();
                            toDisplay = "Player 1 Scored " + DisplayedScore + "!<br>Player 2 Scored " + DisplayedScore2 + "!<br>Together, You Reached Level " + DisplayedLevel + "!";
                        } else {
                            DisplayedLevel = Avoid.level;
                            if(Avoid.score > Avoid.score2)
                                toDisplay = "Player 1 Scored " + DisplayedScore + "!<br>Player 2 Scored " + DisplayedScore2 + "!<br>Together, You Reached Level " + DisplayedLevel + "!<br>Congratulations Player 1, You've Won!";
                            if(Avoid.score < Avoid.score2)
                                toDisplay = "Player 1 Scored " + DisplayedScore + "!<br>Player 2 Scored " + DisplayedScore2 + "!<br>Together, You Reached Level " + DisplayedLevel + "!<br>Congratulations Player 2, You've Won!";
                            if(Avoid.score == Avoid.score2)
                                toDisplay = "Player 1 Scored " + DisplayedScore + "!<br>Player 2 Scored " + DisplayedScore2 + "!<br>Together, You Reached Level " + DisplayedLevel + "!<br>Congratulations, Draw!";
                        }
                    }
                }
            }

            scoreDiv.innerHTML = toDisplay;

            menuDiv.style.display = "" + "block";
            copyrightNotices.style.display = "" + "block";
            scoreDiv.style.display = "" + "block";
        }
    }
);

function playTick() {
    if(tick.currentTime >= 0.02 || tick.currentTime == 0) {
        tick.pause();
        tick.currentTime = 0;
        tick.play();
    }
}

function showSplashScreen() {
    splashDiv.style.display = "" + "block";
    if(splashOpacity < 1) {
        splashOpacity += 0.02;
        splashImg.style.opacity = "" + splashOpacity;
        requestAnimationFrame(showSplashScreen);
    } else {
        splashOpacity = 1;
        splashCount += 1;
        if(splashCount <= 60) {
            requestAnimationFrame(showSplashScreen);
        } else {
            hideSplashScreen();
        }
    }
}

function hideSplashScreen() {
    if(splashOpacity > 0) {
        splashOpacity -= 0.02;
        splashImg.style.opacity = "" + splashOpacity;
        requestAnimationFrame(hideSplashScreen);
    } else {
        splashOpacity = 0;
        splashDiv.style.display = "" + "none";
        menuDiv.style.opacity = "" + 0;
        menuDiv.style.display = "" + "block";
        showMenuDiv();
    }
}

function showMenuDiv() {
    if(splashOpacity < 1) {
        splashOpacity += 0.02;
        menuDiv.style.opacity = "" + splashOpacity;
        requestAnimationFrame(showMenuDiv);
    } else {
        splashOpacity = 1;
        menuDiv.style.opacity = "" + splashOpacity;
    }
}

startBtn.onclick = function() {
    singlePlayer = true;
    startGame();
}
start2Btn.onclick = function() {
    singlePlayer = false;
    startGame();
}

startMusic = function() {
    Avoid.BckMusic.play();
}

function startGame() {
    menuDiv.style.display = "" + "none";
    copyrightNotices.style.display = "" + "none";
    DisplayedScore = 0;
    DisplayedScore2 = 0;
    DisplayedLevel = 0;
    Avoid.Reset();
    Avoid.Start(!singlePlayer);
}

window.onload = function () {
    Avoid.StartEffects();
    showSplashScreen();
}

document.onkeydown = function (e) {
    switch (e.keyCode) {
        case 38: // up arrow
            Avoid.input.up = true;
            break;
        case 40: // down arrow
            Avoid.input.down = true;
            break;
        case 37: // left arrow
            Avoid.input.left = true;
            break;
        case 39: // right arrow
            Avoid.input.right = true;
            break;

        case 87: // w
            Avoid.input.up2 = true;
            break;
        case 83: // s
            Avoid.input.down2 = true;
            break;
        case 65: // a
            Avoid.input.left2 = true;
            break;
        case 68: // d
            Avoid.input.right2 = true;
            break;

        case 13: //enter key
            if(Avoid.isRunning == false) {
                singlePlayer = true;
                startGame();
            }
            break;
    }
}

document.onkeyup = function (e) {
    switch (e.keyCode) {
        case 38: // up
            Avoid.input.up = false;
            break;
        case 40: // down
            Avoid.input.down = false;
            break;
        case 37: // left
            Avoid.input.left = false;
            break;
        case 39: // right
            Avoid.input.right = false;
            break;
            
        case 87: // w
            Avoid.input.up2 = false;
            break;
        case 83: // s
            Avoid.input.down2 = false;
            break;
        case 65: // a
            Avoid.input.left2 = false;
            break;
        case 68: // d
            Avoid.input.right2 = false;
            break;
    }
}
